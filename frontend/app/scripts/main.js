$(window).on('scroll', function() {
  if($(window).scrollTop() > ($('#banner').height() - 105)) {
      $('.header').addClass('active');
  } else {
      //remove the background property so it comes transparent again (defined in your css)
     $('.header').removeClass('active');
  }
});

$('.slide-test').slick({
  dots: true,
  autoplay: true,
  autoplaySpeed: 2000,
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
  ]
});

// $('.responsive').slick({
//   dots: true,
//   speed: 300,
//   slidesToShow: 4,
//   slidesToScroll: 4,
//   responsive: [
//     {
//       breakpoint: 1024,
//       settings: {
//         slidesToShow: 1,
//         slidesToScroll: 1,
//         infinite: true,
//         dots: true
//       }
//     },
//     {
//       breakpoint: 600,
//       settings: {
//         slidesToShow: 2,
//         slidesToScroll: 2
//       }
//     },
//     {
//       breakpoint: 480,
//       settings: {
//         slidesToShow: 1,
//         slidesToScroll: 1
//       }
//     }
//   ]
// });
